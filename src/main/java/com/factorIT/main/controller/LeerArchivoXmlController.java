package com.factorIT.main.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.factorIT.main.iservice.ILeerArchivosXmlService;

@RestController
@RequestMapping("app")
public class LeerArchivoXmlController {
	
	@Autowired
	private ILeerArchivosXmlService leerArchivo;
		
	@PostMapping({"/crearArchivo"})
	public boolean crearXls()throws Exception{
		return leerArchivo.generarExcel();
	}
}
