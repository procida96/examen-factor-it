package com.factorIT.main.dto;

import java.util.ArrayList;
import java.util.List;

public class Empresa {

	private Integer nroContrato;
	private String cuit;
	private String denominacion;
	private String domicilio;
	private Integer codigoPostal;
	private String fechaDesdeNov;
	private String fechaHastaNov;
	private Integer organizador;
	private String productor;
	private Integer ciiu;
	private List<Movimiento> lista;

	public Empresa(Integer nroContrato, String cuit, String denominacion, String domicilio, Integer codigoPostal,
			String fechaDesdeNov, String fechaHastaNov, Integer organizador, String productor, Integer ciiu,
			Movimiento movimiento) {
		this.nroContrato = nroContrato;
		this.cuit = cuit;
		this.denominacion = denominacion;
		this.domicilio = domicilio;
		this.codigoPostal = codigoPostal;
		this.fechaDesdeNov = fechaDesdeNov;
		this.fechaHastaNov = fechaHastaNov;
		this.organizador = organizador;
		this.productor = productor;
		this.ciiu = ciiu;
		this.lista.add(movimiento);
	}

	public Empresa() {
		this.lista = new ArrayList<Movimiento>();
	}

	@Override
	public String toString() {
		return "Empresa [nroContrato=" + nroContrato + ", cuit=" + cuit + ", denominacion=" + denominacion
				+ ", domicilio=" + domicilio + ", codigoPostal=" + codigoPostal + ", fechaDesdeNov=" + fechaDesdeNov
				+ ", fechaHastaNov=" + fechaHastaNov + ", organizador=" + organizador + ", productor=" + productor
				+ ", ciiu=" + ciiu + ", Movimientos=" + lista + "]";
	}

	public Integer getNroContrato() {
		return nroContrato;
	}

	public void setNroContrato(int nroContrato) {
		this.nroContrato = nroContrato;
	}

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

	public String getDenominacion() {
		return denominacion;
	}

	public void setDenominacion(String denominacion) {
		this.denominacion = denominacion;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public int getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(int codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getFechaDesdeNov() {
		return fechaDesdeNov;
	}

	public void setFechaDesdeNov(String fechaDesdeNov) {
		this.fechaDesdeNov = fechaDesdeNov;
	}

	public String getFechaHastaNov() {
		return fechaHastaNov;
	}

	public void setFechaHastaNov(String fechaHastaNov) {
		this.fechaHastaNov = fechaHastaNov;
	}

	public Integer getOrganizador() {
		return organizador;
	}

	public void setOrganizador(int organizador) {
		this.organizador = organizador;
	}

	public String getProductor() {
		return productor;
	}

	public void setProductor(String productor) {
		this.productor = productor;
	}

	public Integer getCiiu() {
		return ciiu;
	}

	public void setCiiu(int ciuu) {
		this.ciiu = ciuu;
	}

	public List<Movimiento> getMovimiento() {
		return lista;
	}

	public void setMovimiento(Movimiento movimiento) {
		this.lista.add(movimiento);
	}

}
