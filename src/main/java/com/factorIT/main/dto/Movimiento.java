package com.factorIT.main.dto;

public class Movimiento {

	private Integer nroContrato;
	private Double saldoCtaCorriente;
	private String tipo;
	private String codigoMovimiento;
	private String concepto;
	private Double importe;

	public Movimiento(Double saldoCtaCorriente, String tipo, String codigoMovimiento, String concepto, Double importe) {
		this.saldoCtaCorriente = saldoCtaCorriente;
		this.tipo = tipo;
		this.codigoMovimiento = codigoMovimiento;
		this.concepto = concepto;
		this.importe = importe;
	}

	public Movimiento() {

	}

	@Override
	public String toString() {
		return "Movimiento [nroContrato=" + nroContrato + ", saldoCtaCorriente=" + saldoCtaCorriente + ", tipo=" + tipo
				+ ", codigoMovimiento=" + codigoMovimiento + ", concepto=" + concepto + ", importe=" + importe + "]";
	}

	public Double getSaldoCtaCorriente() {
		return saldoCtaCorriente;
	}

	public void setSaldoCtaCorriente(Double saldoCtaCorriente) {
		this.saldoCtaCorriente = saldoCtaCorriente;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getCodigoMovimiento() {
		return codigoMovimiento;
	}

	public void setCodigoMovimiento(String codigoMovimiento) {
		this.codigoMovimiento = codigoMovimiento;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	public Integer getNroContrato() {
		return nroContrato;
	}

	public void setNroContrato(Integer nroContrato) {
		this.nroContrato = nroContrato;
	}

}
