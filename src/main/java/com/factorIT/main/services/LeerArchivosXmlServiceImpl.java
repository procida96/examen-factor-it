package com.factorIT.main.services;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.factorIT.main.dto.Empresa;
import com.factorIT.main.dto.Movimiento;
import com.factorIT.main.iservice.ILeerArchivosXmlService;



@Service
public class LeerArchivosXmlServiceImpl implements ILeerArchivosXmlService {

	private List<Empresa> procesarXml() {
		try {
			File archivoXml = new File("src\\main\\resources\\Examen-FIT.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = dbFactory.newDocumentBuilder();
			Document documentoXml = builder.parse(archivoXml);
			documentoXml.getDocumentElement().normalize();
			NodeList listaEmpresas = documentoXml.getElementsByTagName("Empresa");
			List<Empresa> listaEmpre = new ArrayList<Empresa>();
			for (int i = 0; i < listaEmpresas.getLength(); i++) {
				Empresa empresa = new Empresa();
				Movimiento movimiento = new Movimiento();
				Node nodoEmpresa = listaEmpresas.item(i);
				if (nodoEmpresa.getNodeType() == Node.ELEMENT_NODE) {

					Element element = (Element) nodoEmpresa;
					empresa.setNroContrato(
							Integer.parseInt(element.getElementsByTagName("NroContrato").item(0).getTextContent()));
					empresa.setCuit(element.getElementsByTagName("CUIT").item(0).getTextContent());
					empresa.setDenominacion(element.getElementsByTagName("Denominacion").item(0).getTextContent());
					empresa.setDomicilio(element.getElementsByTagName("Domicilio").item(0).getTextContent());
					empresa.setCodigoPostal(
							Integer.parseInt(element.getElementsByTagName("CodigoPostal").item(0).getTextContent()));
					empresa.setFechaDesdeNov(element.getElementsByTagName("FechaDesdeNov").item(0).getTextContent());
					empresa.setFechaHastaNov(element.getElementsByTagName("FechaHastaNov").item(0).getTextContent());
					empresa.setOrganizador(
							Integer.parseInt(element.getElementsByTagName("Organizador").item(0).getTextContent()));
					empresa.setProductor(element.getElementsByTagName("Productor").item(0).getTextContent());
					empresa.setCiiu(Integer.parseInt(element.getElementsByTagName("CIIU").item(0).getTextContent()));

					NodeList listaMov = element.getElementsByTagName("Movimiento");
					for (int j = 0; j < listaMov.getLength(); j++) {
						Node nodoMov = listaMov.item(j);
						if (nodoMov.getNodeType() == Node.ELEMENT_NODE) {
							Element elementMov = (Element) nodoMov;
							movimiento.setNroContrato(Integer
									.parseInt(element.getElementsByTagName("NroContrato").item(0).getTextContent()));
							movimiento.setCodigoMovimiento(
									elementMov.getElementsByTagName("CodigoMovimiento").item(0).getTextContent());
							movimiento
									.setConcepto(elementMov.getElementsByTagName("Concepto").item(0).getTextContent());
							movimiento.setImporte(Double
									.parseDouble(elementMov.getElementsByTagName("Importe").item(0).getTextContent()));
							movimiento.setSaldoCtaCorriente(Double
									.valueOf(elementMov.getElementsByTagName("SaldoCtaCte").item(0).getTextContent()));
							movimiento.setTipo(elementMov.getElementsByTagName("Tipo").item(0).getTextContent());
							empresa.setMovimiento(movimiento);
						}
					}
					listaEmpre.add(empresa);
				}
			}
			return listaEmpre;
		} catch (Exception e) {
		}
		return null;
	}

	@Override
	public boolean generarExcel() {
		Workbook libro = new XSSFWorkbook();
		Sheet hoja = libro.createSheet("Empresas");
		Sheet hoja2 = libro.createSheet("Movimientos");
		String[] cabecerasEmpresa = new String[] { "NroContrato", "CUIT", "DENOMINACION", "DOMICILIO", "CODIGOPOSTAL",
				"PRODUCTOR" };
		String[] cabeceraMovimientos = new String[] { "NroContrato", "SaldoCtaCte", "Concepto", "Importe" };
		List<Movimiento> listaAuxiliar = new ArrayList<Movimiento>();
		int numFila = 1;
		int numFilaMov = 1;
		Row filaEmpresas0 = hoja.createRow(0);

//		CABECERA - EMPRESAS
		Row filaCabeceras = hoja.createRow(0);
		for (int i = 0; i < cabecerasEmpresa.length; i++) {
			Cell celda = filaCabeceras.createCell(i);
			celda.setCellValue(cabecerasEmpresa[i]);
		}

//		CABECERA - MOVIMIENTOS
		Row filaMovimientos0 = hoja2.createRow(0);
		for (int i = 0; i < cabeceraMovimientos.length; i++) {
			Cell celda = filaMovimientos0.createCell(i);
			celda.setCellValue(cabeceraMovimientos[i]);
		}
		for (int i = 0; i < this.procesarXml().size(); i++) {
			Row filaDatos = hoja.createRow(numFila);
			Cell celda0 = filaDatos.createCell(0);
			Cell celda1 = filaDatos.createCell(1);
			Cell celda2 = filaDatos.createCell(2);
			Cell celda3 = filaDatos.createCell(3);
			Cell celda4 = filaDatos.createCell(4);
			Cell celda5 = filaDatos.createCell(5);

			celda0.setCellValue(this.procesarXml().get(i).getNroContrato());
			celda1.setCellValue(this.procesarXml().get(i).getCodigoPostal());
			celda2.setCellValue(this.procesarXml().get(i).getCuit());
			celda3.setCellValue(this.procesarXml().get(i).getDenominacion());
			celda4.setCellValue(this.procesarXml().get(i).getDomicilio());
			celda5.setCellValue(this.procesarXml().get(i).getProductor());

			listaAuxiliar = this.procesarXml().get(i).getMovimiento();
			for (int j = 0; j < listaAuxiliar.size(); j++) {
				Row filaDatosMov = hoja2.createRow(numFilaMov);
				Cell celdaM0 = filaDatosMov.createCell(0);
				Cell celdaM1 = filaDatosMov.createCell(1);
				Cell celdaM2 = filaDatosMov.createCell(2);
				Cell celdaM3 = filaDatosMov.createCell(3);

				celdaM0.setCellValue(listaAuxiliar.get(j).getNroContrato());
				celdaM1.setCellValue(listaAuxiliar.get(j).getSaldoCtaCorriente());
				celdaM2.setCellValue(listaAuxiliar.get(j).getConcepto());
				celdaM3.setCellValue(listaAuxiliar.get(j).getImporte());
				numFilaMov++;
			}
			numFila++;
		}
		try {
			FileOutputStream archivo = new FileOutputStream("Examen.xlsx");
			libro.write(archivo);
			archivo.close();

		} catch (Exception e) {
			System.err.println("Error, " + e);
		}

		return false;
	}

}
